TTFText for Unity 1.0
#################################################################
Created by ComputerDreams.org
Authors : B.Nouvel, O.Blanc
################################################################
Latest infos available from http://unityttf.computerdreams.org/
################################################################

This folder contains three simple demos for Unity TTF Text Package.

To visualize these demos correctly we recommend to switch your "Player Settings" to "Deferred Lighning".
Moreover, because we are dealing with text : Antialias is strongly desired and we hope that you are 
having a Unity Pro version to play with them.

So three demos :

Scene 1 : It is simply an example of static mesh. It has been generated from the editor. It does not move.
Scene 2 : It is a dynamical scene (it works only on Mac and PC with current version of TTF text), 
          It is a particle generator sending letters (but you can change it to be words you like), 
		  using all the fonts available on your system, and demonstrating the capacity of TTFText by 
		  applying different random extrusions to them.
Scene 3 : It is an example of very simple text animation, in this case the mesh is actually statically generated,
          but one subobject have been generated for each characted based on a prefab, and by the mean of this 
		  prefab we animat independently  each character (simply scaling it along its extrusion here).
