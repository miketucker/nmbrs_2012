using UnityEngine;
using System.Collections;

public class AfterSplit : MonoBehaviour {

	private float t = 0.0f;
	private float life = 10.0f;
	// Use this for initialization
	void Start () {
		t = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time - t > life)
		{
			Debug.Log("destroy me!");
			Destroy(gameObject);
		}
	}
}
