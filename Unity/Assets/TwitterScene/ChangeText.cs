using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangeText : MonoBehaviour {
	public GUIText debug;
	public GameObject prefab;	
	public Transform target;
	public float forwardAmount = 100;
	public TwitterFeed feed;
	private List<string> tweetQueue = new List<string>();
	private List<string> tweetsPlayed = new List<string>();
	// Use this for initialization

	private bool isRunning = false;


	void Start () {


		// StartCoroutine("MakeText");
	}

	void Log(string str)
	{
		debug.text = str + "\r\n" + debug.text;
	}

	public void TweetsLoaded()
	{

		AddNewTweets(feed.tweets);
		Log("tweets loaded received");

		if(!isRunning)
		{
			StartCoroutine("MakeText");
			isRunning = true;
		}
	}

	private void AddNewTweets(List<string> tweets)
	{

		Log("add new tweets "+tweets.Count);

		foreach(string tweet in tweets)
		{

			int i = tweetsPlayed.FindIndex(delegate (string s) { return s == tweet; });
			Log("find played: "+i);
			if(i >= 0) continue;

			i = tweetQueue.FindIndex(delegate (string s) { return s == tweet; });
			Log("find queued: "+i);
			if(i == -1)
			{
				Log("add new tweet: "+tweet);
				tweetQueue.Add(tweet);
			}
		}
	}

	IEnumerator MakeText()
	{
		yield return new WaitForSeconds(.5f);
		while(true)
		{
			if(tweetQueue.Count > 0)
			{
				GameObject go = GameObject.Instantiate(prefab) as GameObject;
				MeshCollider mc = go.AddComponent<MeshCollider>();
				mc.convex = true;
				go.transform.position = target.position + Vector3.up * 3.0f + Vector3.forward * forwardAmount;
				TTFText txt = go.GetComponent<TTFText>();
				txt.Text = tweetQueue[0];
				tweetQueue.RemoveAt(0);
				tweetsPlayed.Add(tweetQueue[0]);
			}
			yield return new WaitForSeconds(5.0f);
		}


	}
}
