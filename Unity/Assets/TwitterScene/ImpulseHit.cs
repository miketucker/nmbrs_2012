using UnityEngine;
using System.Collections;

public class ImpulseHit : MonoBehaviour {
	public SphereCollider sphereCollider;
	public float amp = 5.0f;
	public float cooldown = 3.0f;
	public float destroyTime = 2.0f;
	private float rad;
	private float lastHit;
	public GameObject floor;
	public float impulseForce = 10.0f;
	public Vector3 direction = new Vector3(0,.5f,1f);
	// Use this for initialization
	void Start () {
		if(sphereCollider!=null) rad = sphereCollider.radius;
	}
	
	// Update is called once per frame
	void OnCollisionEnter(Collision c)
	{
		// Debug.Log("collision "+c);
		if(c.gameObject == floor) return;
		if(Time.time - lastHit > cooldown )
		{
			// Debug.Log("Shatter "+ c.gameObject.name);
			// foreach(Transform t in c.gameObject.transform)
			// {
					
			// }
			c.gameObject.rigidbody.AddForceAtPosition(direction * impulseForce,c.contacts[0].point,ForceMode.Impulse);


			StartCoroutine("Deflate");
			lastHit = Time.time;
			// Debug.Break();
		}
	}

	void Hit(GameObject g)
	{
		// g.collider.SendMessage("Shatter", g.transform.position, SendMessageOptions.DontRequireReceiver);
		g.rigidbody.AddForceAtPosition(direction * impulseForce,g.transform.position,ForceMode.Impulse);
		if(sphereCollider!=null) sphereCollider.radius = rad * amp;
		StartCoroutine(Cleanup(g));
	}

	IEnumerator Cleanup(GameObject g)
	{
		yield return new WaitForSeconds(destroyTime);
		Destroy(g);
	}

	IEnumerator Deflate()
	{
		yield return new WaitForSeconds(.5f);
		if(sphereCollider!=null) sphereCollider.radius = rad;
	}
}
