using UnityEngine;
using System.Collections;

public class Spear : MonoBehaviour {
	public float maxDist = 20.0f;
	public float freq = 3.0f;
	public float vary = 0.05f;
	public LayerMask mask;
	// Use this for initialization
	void Start () {
		StartCoroutine("Loop");
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.white;
		Vector3 rnd = new Vector3(Random.Range(-.5f,.5f),Random.Range(0,.5f),0) * vary;
		Gizmos.DrawLine(transform.position,transform.position + (transform.forward + rnd) * maxDist);
	}
	
	// Update is called once per frame
	IEnumerator Loop () {
		while(true)
		{
			RaycastHit hit;
			Debug.Log("speer!");

			Vector3 rnd = new Vector3(Random.Range(-.5f,.5f),Random.Range(0,.5f),0) * vary;

			if(Physics.Raycast(transform.position,transform.forward + rnd,out hit, maxDist,mask.value))
			{
				// Debug.Log("speer hit!"+ hit.collider.gameObject);
				if(hit.collider != null)
				{
					Debug.Log("hit!");
					hit.collider.SendMessage("Shatter", hit.point, SendMessageOptions.DontRequireReceiver);
				}
			}
			yield return new WaitForSeconds(freq);
		}
	}
}
