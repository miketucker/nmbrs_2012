#if UNITY_3_5_2 || UNITY_3_6 || UNITY_4_0
#if UNITY_STANDALONE_MAC || UNITY_STANDALONE_PC
using UnityEditor.Callbacks;
using System;


public class TTFTextBuildPostprocessor {
[PostProcessBuild]
public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {

		
foreach(TTFontInfo fi in TTFTextFontListManager.Instance.LocalFonts) {
		System.IO.File.Copy(fi.Path);		
}
		
		
}
}

#endif
#endif