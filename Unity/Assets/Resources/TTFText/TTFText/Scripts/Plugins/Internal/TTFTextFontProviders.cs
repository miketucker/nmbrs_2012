using UnityEngine;
using System.Collections.Generic;

public interface FontProvider {
	List<string> GetFontList(object parameters);
	
	bool IsCompatible(UnityEngine.RuntimePlatform p);
	object GetFont(object parameters,string fontid);
	//object GetFont(string fontid, bool bold, bool italic);
	object DefaultFontSelector(object parameters,params string [] fontid);
	
	object GetFont(object parameters,object fontselector);	
	TTFTextOutline GetGlyphOutline(object parameters,object font,char c);
	Vector3 GetAdvance(object parameters,object font, char c);
	float GetHeight(object parameters, object font);
	void DisposeFont(object font);
	void RegisterClient(TTFText t);
	void UnregisterClient(TTFText t);
	void IncRef(object parameters, string fontid);
	void DecRef(object parameters, string fontid);
}



public class FreeTypeFontProvider  : FontProvider {
	public class Parameters {
		//[TTFTextParamValidValue(i=>(i<1)?1:((i>30)?30:i))]
		public bool showSystemFonts=true;
		public int Interpolation=4;
		public string FallbackFonts="Arial (Regular);Helvetica (Regular);Times (Regular)";
	}
	
	
	public Parameters parameters=new Parameters();
	
	public class FontSelector {
		public string [] fonts;
		public int interpolation;
	}
	
	public class Font {
		public bool reversed;
		public FTSharp.Font font;		
		public Font(FTSharp.Font f) {font=f; reversed=false;}
		public Font(FTSharp.Font f,bool r) {font=f; reversed=r;}
	}
	
	public object DefaultFontSelector(object parameters,params string [] fontids) {
		return fontids;
	}
	
	public bool IsCompatible(UnityEngine.RuntimePlatform p) {
		if ((p==UnityEngine.RuntimePlatform.WindowsEditor) 
			||(p==UnityEngine.RuntimePlatform.WindowsPlayer)
			||(p==UnityEngine.RuntimePlatform.OSXEditor)
			||(p==UnityEngine.RuntimePlatform.OSXPlayer)
			//||(p==UnityEngine.RuntimePlatform.OSXDashboardPlayer)
			//||(p==UnityEngine.RuntimePlatform.LinuxPlayer)
			) {			
			return true;
		}
		
		return false;
	}
	
	

	
	public List<string> GetFontList(object parameters) {
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_MOSX|| UNITY_EDITOR	
		List<string> r=new List<string>();
		foreach(var f in TTFTextFontListManager.Instance.LocalFonts) {
			r.Add(f.Value.Name);
		}
		if ((parameters==null)||((parameters as Parameters).showSystemFonts)) {
		foreach(var f in TTFTextFontListManager.Instance.SystemFonts) {
			r.Add(f.Value.Name);
		}
		}
		
		return r;		
#else  
		return null;
#endif		
	}
	
	public object GetFont(object parameters,string fontid) {
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_MOSX|| UNITY_EDITOR			
		bool reversed=false;
		if (fontid.Length==0) {
			return null;
		}
		FTSharp.Font f=TTFTextFontListManager.Instance.OpenFont(fontid,1,ref reversed);
		return new Font(f,reversed);
#else 
		return null;
#endif 		
		
	}
	
	public object GetFont(object parameters,object fontselector) {
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_MOSX|| UNITY_EDITOR			
		string [] fonts=( string [])fontselector;
		foreach(string fontid in fonts) {
			if (fontid.Length==0) {
				continue;
			}

			try {
				
				return GetFont(parameters,fontid);
			}
			catch {} //(System.Exception e) {}
		}
#endif		
		return null;
	}
	
	public TTFTextOutline GetGlyphOutline(object parameters,object font, char c) {
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_MOSX|| UNITY_EDITOR			
		Font fnt=(Font)font;		
		FTSharp.Outline.Point adv=new FTSharp.Outline.Point();
		FTSharp.Outline ol=fnt.font.GetGlyphOutline(c);
		
		if (fnt.reversed) {
			return new TTFTextOutline(ol,adv,true);
		}
		else {
			return new TTFTextOutline(ol,adv,false);
		}
#else
		return null;
#endif		
	}
	
	public Vector3 GetAdvance(object parameters,object font, char c) {
#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_MOSX|| UNITY_EDITOR			
		FTSharp.Font fnt=((Font) font).font;
		FTSharp.Outline.Point adv;
		// TODO: EXPORT SYMBOL TO GET ONLY ADV
		fnt.GetGlyphOutline(c,out adv);
		return new Vector3(adv.X,adv.Y,0);
#else
		return Vector3.zero;
#endif		
	}
	
	public float GetHeight(object parameters, object font) {
		FTSharp.Font fnt=((Font) font).font;
		return fnt.Height;
	}
	
	public void DisposeFont(object font) {
		FTSharp.Font fnt=((Font) font).font;
		fnt.Dispose();
	}
	
	public void RegisterClient(TTFText t) {
	}
	public void UnregisterClient(TTFText t) {
	}	
	public void IncRef(object parameters, string fontid) {
	}	
	public void DecRef(object parameters, string fontid) {
	}
	
}

#if ! TTFTEXT_LITE
public class FontStoreFontProvider: FontProvider  {
	public class Parameters {
		public bool showOnlyAlreadyEmbeddedFonts=false;
		public bool localMode; // local or global fonts ?
		public string additionalCharacters;
	}
	public Parameters parameters=new Parameters();
	
	static TTFTextFontStore _fontstore;
	static TTFTextFontStore fontstore {
		get { if (_fontstore==null) {
				_fontstore=TTFTextFontStore.Instance;
			}
			return _fontstore;
		}
	}
	
	public bool IsCompatible(UnityEngine.RuntimePlatform p) {
			return true;
	}
	
	public List<string> GetFontList(object parameters) {		
		//TTFTextFontStore.Instance
		System.Collections.Generic.List<string> r=new System.Collections.Generic.List<string>();
		if (TTFTextFontStore.IsInstanciated) {
			foreach(TTFTextFontStoreFont f in  TTFTextFontStore.Instance.embeddedFonts) {
				r.Add(f.fontid);
			}
		}
		if (!(parameters as Parameters).showOnlyAlreadyEmbeddedFonts) {
			r.AddRange(TTFTextFontProvider.font_providers[0].GetFontList(null));
		}
		return r;
	}

	
	public object DefaultFontSelector(object parameters,params string [] strings) {
		return null;
	}
	
	public object GetFont(object parameters,string fontid) {
		return fontstore.GetFont(fontid);
	}

	public object GetFont(object parameters,object fontselector) {
		return fontstore.GetFont(fontselector as string);
	}
	
	
	public object GetFont(object parameters,string fontid, bool bold, bool italic) {
		return fontstore.GetFont(fontid);
	}
	
	public TTFTextOutline GetGlyphOutline(object parameters,object font, char c) {
		return new TTFTextOutline(((TTFTextFontStoreFont)font).GetGlyph(c));
	}
	
	public Vector3 GetAdvance(object parameters,object font, char c) {
		return ((TTFTextFontStoreFont)font).GetAdvance(c);
	}

	public float GetHeight(object parameters, object font) {
		TTFTextFontStoreFont fnt=(TTFTextFontStoreFont) font;
		return fnt.height;
	}
	
	
	public void DisposeFont(object font) {
	}
	public void RegisterClient(TTFText t) {
		TTFTextFontStore store=TTFTextFontStore.Instance;
		store.RegisterClient(t);
	}
	public void UnregisterClient(TTFText t) {
		TTFTextFontStore store=TTFTextFontStore.Instance;
		store.UnregisterClient(t);
	}
	
	public void IncRef(object parameters, string fontid) {
		TTFTextFontStoreFont fnt=TTFTextFontStore.Instance.GetEmbeddedFont(fontid);
		if (fnt==null) {
			fnt=TTFTextFontStore.Instance.EnsureFont(fontid);
			fnt.BuildCharSet(fontid);
		}
		fnt.incref();
	}	
	public void DecRef(object parameters, string fontid) {
		TTFTextFontStoreFont fnt=TTFTextFontStore.Instance.GetEmbeddedFont(fontid);
		if (fnt!=null) {
			fnt.decref();
			TTFTextFontStore.Instance.SetGarbageCollectUnusedFonts();
		}
		
	}
		
}

#if ANDROIDNATIVE_READY			
public class AndroidFontProvider : FontProvider {
	public class Parameters {
		public float InterpolationStep=1;
		public string [] fallbackFonts={"Droid Sans", "Droid Serif", "Droid Monospace"};
	}
	public Parameters parameters=new Parameters();	

	public object DefaultFontSelector(object parameters, params string []xx) { return null;}
	
	public bool IsCompatible(UnityEngine.RuntimePlatform p) {
			return (p==RuntimePlatform.Android);
	}
	
	public List<string> GetFontList(object parameters) {
#if UNITY_ANDROID && !UNITY_EDITOR		
		//return TTFTextInternalAndroid.Instance.GetTypeface(fontid,false,false);
		return TTFTextInternalAndroid.Instance.EnumFonts();
#else	
	   return new List<string>(new string []{"Droid Sans","Droid Serif","Droid Monospace"});
#endif		
		
	}
	
	public object GetFont(object parameters,string fontid) {
#if UNITY_ANDROID		
		return TTFTextInternalAndroid.Instance.GetTypeface(fontid,false,false);
#else	
		return null;	
#endif		
	}
	
	public object GetFont(object parameters,string fontid, bool bold, bool italic) {
#if UNITY_ANDROID		
		return TTFTextInternalAndroid.Instance.GetTypeface(fontid,bold,italic);
#else
		return null;
#endif		
	}

	public object GetFont(object parameters,object xxx) {
		// NOT YET IMPLEMENTED
		return null;
	}
	
	
	
	public TTFTextOutline GetGlyphOutline(object parameters,object font, char c) {
#if UNITY_ANDROID		
		return TTFTextInternalAndroid.Instance.GetGlyph(font as TTFTextInternalAndroid.Font,c);
#else 
		return null;
#endif 		
	}
	
	public Vector3 GetAdvance(object parameters,object font, char c) {
#if UNITY_ANDROID			
		return new Vector3(TTFTextInternalAndroid.Instance.GetGlyphAdvance(font as TTFTextInternalAndroid.Font,c),0,0);
#else
		return Vector3.zero;
#endif		
	}

	
	public void DisposeFont(object font) {
	}
	
	public float GetHeight(object parameters, object font) {		
		return 1;
	}
	public void RegisterClient(TTFText t) {
	}
	public void UnregisterClient(TTFText t) {
	}
	
	public void IncRef(object parameters, string fontid) {
	}	
	public void DecRef(object parameters, string fontid) {
	}
	
	
}
#endif
#endif

/*
public class PolyFontProvider : FontProvider {
	public class Parameters {
		
	}
	public Parameters parameters=new Parameters();	
	
	public class Font {
		public object fontselector;
		private object [] font;
		private int builtfont=-1;
			
		// ---------------------------------------
		public Font(object fs) {fontselector=fs; object [] cfs=(object []) fontselector; font =new object[cfs.Length];} 
	
		public Font(object cfont, int id) {
			builtfont=id;
			font =new object[subproviders.Length];
			font[id]=cfont;
		}
		
		public object GetFont(int i) {
			object [] fs=(object []) fontselector;
			if (font[i]==null) {
				font[i]=subproviders[i].GetFont(fs[i]);
			}
			return font[i];
		}
		
		public object GetFont() {
			if (builtfont!=-1) {
				return font[builtfont];
			}
			for (int i=0;i<subproviders.Length;i++) {
				if (GetFont(i)!=null) {
					builtfont=i;
					return font[i];
				}
			}
			return null;
		}
		
		public int FontProviderId() {return builtfont;}
	}
	
	static FontProvider [] subproviders;	
	public PolyFontProvider(FontProvider []  cp ) {
		subproviders=cp;
	}
	
	
	
	public object DefaultFontSelector( params string [] xx) {
		object [] sels = new object[subproviders.Length];
		for (int i=0;i<sels.Length;i++) {
		  sels[i]=subproviders[i].DefaultFontSelector();
		}		
		return sels;
	}
	
	public bool IsCompatible(UnityEngine.RuntimePlatform p) {
			return true;
	}
	
	public List<string> GetFontList() {
		return null;
	}
	
	public object GetFont(string fontid) {
		for (int i=0;i <subproviders.Length;i++) {
			try {
			  object f=subproviders[i].GetFont(fontid);
			  if (f!=null) {
					return new Font(f,i);
			  }
			}
			catch {}
		}
		return null;
	}
	
	public object GetFont(string fontid, bool bold, bool italic) {
		return null;
	}

	public object GetFont(object o) {
		return new Font(o );
	}

	
	public TTFTextOutline GetGlyphOutline(object font, char c) {
		object f;
		Font F=(font as Font);
		f=F.GetFont();
		return subproviders[F.FontProviderId()].GetGlyphOutline(f,c);
	}
	
	public Vector3 GetAdvance(object font, char c) {
		object f;
		Font F=(font as Font);
		f=F.GetFont();
		return subproviders[F.FontProviderId()].GetAdvance(f,c);
	}
	
}

 */


public class TTFTextFontProvider {
	static FontProvider ftfp=new FreeTypeFontProvider() as FontProvider;
#if !TTFTEXT_LITE	
	static FontProvider fsfp=new FontStoreFontProvider() as FontProvider;
#if ANDROIDNATIVE_READY			
	static FontProvider afp= new AndroidFontProvider() as FontProvider;
#endif
	//static FontProvider pfp= new PolyFontProvider(new FontProvider [] {ftfp,fsfp,afp}) as FontProvider;
#endif	
	static public FontProvider [] font_providers = 
		new FontProvider [] {
		    ftfp
#if !TTFTEXT_LITE		
		    ,fsfp
#if ANDROIDNATIVE_READY		
		    ,afp
#endif		
		// , pfp
#endif		
		};
}
