using UnityEngine;
using System.Collections;

public class TimeUp : MonoBehaviour {
	public Material mat;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	if(mat!=null) mat.SetFloat("_TimeFloat",Time.time);
	}
}
